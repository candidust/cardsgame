/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package API;

import DataMining.CricketPlayerCards;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;
import static jdk.nashorn.internal.objects.NativeArray.reverse;

/**
 * This class manages all the cards collected by the game player
 * @author BatmanLost
 */
class GamePlayerCardsManager {
    
    // All the cricket player cards this player has in his collection
    private final Set<CricketPlayerProfile> ownerCardsCollection;
    
    // store all the cards with each statistic as priority, max PQ's
    private final PriorityQueue<CricketPlayerProfile> PQofRuns;
    private final PriorityQueue<CricketPlayerProfile> PQofMatches;
    private final PriorityQueue<CricketPlayerProfile> PQofAverage;
    private final PriorityQueue<CricketPlayerProfile> PQofHighScore;
    
    // card which is transfered to or from owner most recently
    private CricketPlayerProfile recentlyTansfered;
    
    /**
     * Returns a card manager which answers basic queries 
     * @param ownerCards
     */
    public GamePlayerCardsManager(Set<CricketPlayerProfile> ownerCards){
        // store all cards
        this.ownerCardsCollection = new HashSet<CricketPlayerProfile>(ownerCards);
        
        // initialize data storage
        PQofRuns      = new PriorityQueue<CricketPlayerProfile>(Collections.reverseOrder(CompareCards.BY_RUNS));
        PQofMatches   = new PriorityQueue<CricketPlayerProfile>(Collections.reverseOrder(CompareCards.BY_MATCHES));
        PQofAverage   = new PriorityQueue<CricketPlayerProfile>(Collections.reverseOrder(CompareCards.BY_AVERAGE));
        PQofHighScore = new PriorityQueue<CricketPlayerProfile>(Collections.reverseOrder(CompareCards.BY_HIGH_SCORE));
        
        // store data
        addAllCards();
    }

    /**
     * Stores given data 
     * @param ownerCardsCollection 
     */
    private void addAllCards() {
        PQofRuns.addAll(ownerCardsCollection);
        PQofMatches.addAll(ownerCardsCollection);
        PQofAverage.addAll(ownerCardsCollection);
        PQofHighScore.addAll(ownerCardsCollection);
    }
    
    /**
     * Returns all the cards of owner as a set
     * @return 
     */
    public Set<CricketPlayerProfile> getAllCards(){
        return ownerCardsCollection;
    }
    
    /**
     * Returns a random card from owner's collection
     * @return 
     */
    public CricketPlayerProfile getRandomCard(){
        int N = (int)((Math.random())*(ownerCardsCollection.size()));        
        int i=0;
        CricketPlayerProfile randomCard = null;
        for(CricketPlayerProfile cricketCard: ownerCardsCollection){
            if(i==N){ randomCard = cricketCard; break;}
            i++;
        } 
        return randomCard;
    }
    
    public CricketPlayerProfile getRecentTransfer(){ return recentlyTansfered;}
    
    /**
     * Adds a new card to owner's collection on winning a challenge
     * @param card
     */
    public void addNewCard(CricketPlayerProfile card){
        recentlyTansfered = card;
        
        ownerCardsCollection.add(card);
        PQofRuns.add(card);
        PQofMatches.add(card);
        PQofAverage.add(card);
        PQofHighScore.add(card);
    }
    
    /**
     * Removes the card lost from owner's collection
     * @param lostCard 
     */
    public void remove(CricketPlayerProfile lostCard){
        recentlyTansfered = lostCard;
        
        ownerCardsCollection.remove(lostCard);
        PQofRuns.remove(lostCard);
        PQofMatches.remove(lostCard);
        PQofAverage.remove(lostCard);
        PQofHighScore.remove(lostCard);
        
    }
    
    /**
     * Returns number of cricket cards in owner's collection
     * @return 
     */
    public int cardsCount(){
        return ownerCardsCollection.size();
    }
    
    /**
     * Returns best card based on @param statistic as requested by owner
     * @param compareStat
     * @return 
     */
    public CricketPlayerProfile getBestCardBy(String compareStat){
        return getCorrespondingPQ(compareStat).peek();
    }
    
    /**
     * Returns priority queue as requested
     * @param compareStat
     * @return 
     */
    private PriorityQueue<CricketPlayerProfile> getCorrespondingPQ(String compareStat){
        if(compareStat.equals("Runs")){ return PQofRuns;}
        else if(compareStat.equals("Matches")){return PQofMatches;}
        else if(compareStat.equals("Average")){return PQofAverage;}
        else{ return PQofHighScore;}
    }
    /**
     * Returns a sorted array of cards as requested by the owner
     * @param compareStat
     * @param minFirst
     * @return 
     */
    public CricketPlayerProfile[] getCardsSortedBy(String compareStat, boolean minFirst){
        CricketPlayerProfile[] maxSortedCards = popAll(getCorrespondingPQ(compareStat));
        if(minFirst){ return reverse(maxSortedCards);}
        else{ return maxSortedCards;}
    }
    /**
     * Reverses the array
     * @param array
     * @return 
     */
    private CricketPlayerProfile[] reverse(CricketPlayerProfile[] array){
        int N = array.length;
        CricketPlayerProfile[] reverseArray = new CricketPlayerProfile[N];
        for(int i=0; i<N;i++){
            reverseArray[i] = array[N-1-i];
        }
        return reverseArray;
    }
    /**
     * Helper method 
     * @param cards
     * @return 
     */
    private static CricketPlayerProfile[] popAll(PriorityQueue<CricketPlayerProfile> cards){
        PriorityQueue<CricketPlayerProfile> cardsCopy  = new PriorityQueue<CricketPlayerProfile>(cards);
        int size = cards.size();
        CricketPlayerProfile[] cardsArray = new CricketPlayerProfile[size];
        for(int i=0; i<size; i++){
            cardsArray[i] = cardsCopy.poll();
        }
        return cardsArray;
    }
}
