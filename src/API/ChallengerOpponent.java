/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package API;

/**
 * This class simulates game every time challenge button is clicked, kind of referee
 * @author BatmanLost
 */
public class ChallengerOpponent{
    
    // two players playing the game, static because players don't change after the game has started
    
    // one player is challenger and the other is opponent
    private static GamePlayer challenger;
    private static GamePlayer opponent; 
    
    // predicate to swap challenger and opponent after a challenge
    private static boolean swap;  
    

    /**
     * Assigns pointers to both players
     * @param player
     * @param computer 
     */
    public ChallengerOpponent(GamePlayer player1, GamePlayer player2) {
        challenger = player1;
        opponent = player2;
    }
    
    /**
     * Takes a challenge and sets appropriate pointers 
     * @param newChallenger 
     */
    public static void processNewChallenge(GamePlayer newChallenger){
        if(challenger != newChallenger){
            GamePlayer temp = challenger;
            challenger = newChallenger;
            opponent = temp;
        }
        updateGameProgress();
    }

    /**
     * Returns true if challenger has lost the recent challenge
     * @return 
     */
    public static boolean swap(){ return swap;}
    
    /**
     * Returns true if challenger has won recent challenge
     * @return 
     */
    public boolean isChallengerWinner(){ return !swap;}
    
    /**
     * Returns the current challenger
     * @return 
     */
    public  GamePlayer getChallenger(){ return challenger;}
    
    /**
     * Returns the current opponent
     * @return 
     */
    public  GamePlayer getOpponent(){ return opponent;}
    
    private static void updateGameProgress(){
        // temp variable
        boolean swapChallenger;
        
        // Notify opponent to prepare his strike player
        // if challenger has selected same stat i.e, which opponent selected as defense
        // opponent gets his best card for that stat as strike cricket player, otherwise
        // a random strike cricket player is selected for opponent
        opponent.prepareStrikePlayer(challenger.challengeWith());
        
        // compare the two strike cards of challenger and opponent w.r.t a given stat
        int result = CompareCards.compare(challenger.getStrikeCricketPlayer(), opponent.getStrikeCricketPlayer(),challenger.challengeWith());
        // if challenger has lost the challenge, he gives his strike card to opponent
        if(result < 0){ 
            challenger.TransferCardTo(opponent);
            swapChallenger = true;
        }
        //if challenger won the challenge, he takes opponent's strike card
        else if(result >0){ 
            opponent.TransferCardTo(challenger);            
            swapChallenger = false;
        }
        // if it's a tie
        else{ 
            swapChallenger = false; //-------------- <<<<<>>>>>>-------------  
        }
        swap = swapChallenger;
    }
    
    /**
     * Updates both player's strike cards to random cards
     */
    public static void updateStrikePlayers(){
        challenger.setStrikePlayerAtRandom();
        opponent.setStrikePlayerAtRandom();
    }
    
    
}
