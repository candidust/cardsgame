/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package API;

import DataMining.CricketPlayerCards;
import java.util.HashMap;
import java.util.Set;

/**
 * This class maintains the profile of the player playing this game
 * @author BatmanLost
 */
public class GamePlayer {
    
    // Name of this player
    private final String name;    
    // Card with which this player is going to play next i.e, strike player
    private CricketPlayerProfile headOfDeck;
    // Cards manager for this player
    private GamePlayerCardsManager cardsManager;
    
    // Going to challenge with this stat of Strike cricket player
    private String challengeWithStat; 
    
    // Going to tackle with best player of this stat
    private String tackleWithStat;  
    
    
    /**
     * Create a new Player of this game with at least one cricket player card in his collection
     * @param cricketCards 
     * @param name 
     */
    public GamePlayer(Set<CricketPlayerProfile> cricketCards, String name){
        
        this.name = name.toUpperCase();
        //Initially provide this user with some cards
        this.cardsManager = new GamePlayerCardsManager(cricketCards);
        this.headOfDeck = cardsManager.getRandomCard();
    }
    
    /**
     * Transfers the card to winner of the challenge and updates strike cricket player
     * @param receiver 
     */
    public void TransferCardTo(GamePlayer receiver){
        // send card to receiver
        receiver.addCard(headOfDeck);
        // delete sent card from collection
        this.cardsManager.remove(headOfDeck);        
    }
    
    public CricketPlayerProfile getRecentTransferCard(){
        return cardsManager.getRecentTransfer();
    }
    
    /**
     * Updates the strike player after each challenge randomly
     */
    public void setStrikePlayerAtRandom(){
        headOfDeck = cardsManager.getRandomCard();
    }
    
    /**
     * Updates the strike cricketer of this player 
     * @param player
     */
    public void setStrikePlayer(CricketPlayerProfile player){
        headOfDeck = player;       
    }
    
    
    /**
     * Returns number of cricket cards in this players's collection
     * @return 
     */
    public int cardsCount(){
        return cardsManager.cardsCount();
    }
    
    /**
     * Adds a new card to his collection on winning a challenge
     * @param card
     */
    public void addCard(CricketPlayerProfile card){
        cardsManager.addNewCard(card);
    }  
    
    
    /**
     * Set stat of strike cricket player to challenge with
     * @param stat
     */
    public void setChallengeWith(String stat){
        challengeWithStat = stat;
    }
    
    /**
     * Stat of strike cricket player to challenge with
     * @return 
     */
    public String challengeWith(){
        return challengeWithStat;
    }
    
    /**
     * Set stat of strike cricket player to tackle with
     * @param stat
     */
    public void setTackleWith(String stat){
        tackleWithStat = stat;
    }
    
    /**
     * Returns Stat chosen of strike cricket player to tackle with
     * @return 
     */
    public String tackleWith(){
        return tackleWithStat;
    }
    
    /**
     * Returns the Card with which this player is going to play next
     * @return 
     */
    public CricketPlayerProfile getStrikeCricketPlayer(){
        return headOfDeck;
    }
    
    /**
     * If the opponent's selected challenged stat does not match
     * this players selected tackle stat, then a random card is set as strike player
     */
    public void prepareStrikePlayer(String stat){
        if(!tackleWithStat.equals(stat)){ 
            setStrikePlayerAtRandom();
        }
    }

    /**
     * Returns the name of this player
     * @return 
     */
    public String getName() { return name; }
    
    /**
     * Returns the best card this player has based on required statistic
     * @param compareStat
     * @return 
     */
    public CricketPlayerProfile getBestPlayerby(String compareStat){
        return cardsManager.getBestCardBy(compareStat);
    }
    
    public CricketPlayerProfile[] getCardsSortedBy(String stat, boolean minFirst){
        return cardsManager.getCardsSortedBy(stat, minFirst);
    }
    
    /**
     * Returns a table with stat mapped to best player available for that stat
     * @return 
     */
    public HashMap<String,CricketPlayerProfile> getBestPlayers(){
        HashMap<String,CricketPlayerProfile> bestPlayers = new HashMap<String,CricketPlayerProfile>();
        // iterates through all different stats available and maps accordingly
        for(String stat : CricketPlayerProfile.getAllStatsAsStrings()){
            bestPlayers.put(stat, getBestPlayerby(stat));
        }
        return bestPlayers;
    }
    
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Name of Game Player: "+name);
        sb.append("\n Strike Cricket Player :"+ headOfDeck.getName());
        sb.append("\n TotalCards :"+ cardsCount());
        return sb.toString();
    }
    
    
    //************** UI related fields ************//
    // This player requests his own requirements for UI
    
    // UI view this player needs to display
    private String viewRequired;
    // previous view of this player
    private String previousView;
    
    private String sortBy;
    
    private boolean minFirst;  

    public String getViewRequired() {
        return viewRequired;
    }
    
    public String getPreviousView() {
        return previousView;
    }
    
    public void setPreviousViewToTackleChallengeView() {
        this.previousView = "TackleChallengeView";
    }
    
    public void setPreviousViewToWaitForOpponentView() {
        this.previousView = "WaitForOpponentView";
    }
    
    public void setViewRequired(String view) {
        this.previousView = this.viewRequired;
        this.viewRequired = view;
    }

    public void orderWaitForOpponentView() {
        setViewRequired("WaitForOpponentView");
    }
    
    public void orderReadyToTackleView() {
        setViewRequired("ReadyToTackleView");
    }
    
    public void orderTackleChallengeView() {
        setViewRequired("TackleChallengeView");
    }
    
    public void orderPlayerChallengeView() {
        setViewRequired("PlayerChallengeView");
    }
    
    public void orderChallengeWinnerView(){
        setViewRequired("ChallengeWinnerView");
    }
    
    public void orderChallengeLoserView(){
        setViewRequired("ChallengeLoserView");
    }
    
    public void orderCardsCollectionView(){
        setViewRequired("CardsCollectionView");
    }
    
    public void orderModifiedCardsCollectionView(){
        this.viewRequired = "CardsCollectionView";
    }
    
    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public boolean isMinFirst() {
        return minFirst;
    }

    public void setMinFirst(boolean minFirst) {
        this.minFirst = minFirst;
    }
    
}
