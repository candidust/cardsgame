/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package API;

import java.util.HashMap;

/**
 * The CricketPlayerProfile class encapsulates a cricket player data which are 
 * image, name, runs, matches, average and high score.
 * @author BatmanLost
 */
public class CricketPlayerProfile {
    
    /**
     * Helper class to wrap high score with Out/NotOut status
     */
    public static class HighScore implements Comparable<HighScore>{
        private short hs;
        private boolean out;
        //public static final HighScoreComparator HScomparator = new HighScoreComparator();        
        
        public HighScore(short score, boolean outStatus){
            this.hs = score;
            this.out = outStatus;
        }
        
        public String toString(){
            String s = hs+"";
            if(!out){ s+= "*";}
            return s;
        } 
        
        @Override
        /**
         * Player who stays not out has highest priority
         */
        public int compareTo(HighScore that) {            
            if( this.hs > that.hs ) { return 1;}
            else if( this.hs < that.hs) { return -1;}
            else{
                if(!this.out && that.out){ return 1;}
                else if(this.out && !that.out){ return -1;}
                else{ return 0;}
            }
        }
        
        /**
         * Returns Hash Code
         * @return 
         */
        @Override
        public int hashCode(){
            int hash = 17;
            hash = hash*31 + (int)hs;
            hash = hash*31 + ((Boolean) out).hashCode();
            return hash;
        }
    }   
    
    // no of stats
    private static final int STATS_COUNT = 4; 
    
    
    //Player data
    private String name;
    private short runs;
    private short matches;
    private double average;
    private HighScore highScore;   
       
    
    // Cache the hash code
    private volatile int hashCode;
    
    
    public CricketPlayerProfile(){}
    
    /**
     * Get a new Cricket player profile
     * @param name
     * @param runs
     * @param matches
     * @param average
     * @param hs 
     */
    public CricketPlayerProfile (String name, short runs, short matches, double average, HighScore hs ){
        this.name = name;
        this.runs = runs;
        this.matches = matches;
        this.average = average;
        this.highScore = hs;        
    }
    
    /**
     * Returns a cricket player for testing purposes
     * @return 
     */
    public static CricketPlayerProfile getTestCricketPlayerProfile(){
        String name = "Virat Kohli";
        short runs = 10000;
        short matches = 150;
        double average = 24.33;
        HighScore hs = new HighScore((short)168,false);
        return new CricketPlayerProfile(name,runs,matches,average,hs);
    }
    
    /**
     *  Returns a HashMap<String,String> with keys as statistic mapped to it's value
     * @return 
     */
    public HashMap<String,String> getPlayerStats(){
        HashMap<String,String> hashTable = new HashMap<String,String>();    

        for(String stat: getAllStatsAsStrings()){
            hashTable.put(stat, getStat(stat));
        }
        
        return hashTable;
    }
    
    /**
     * Generic helper method to get String value of data
     * @param <T>
     * @param stat
     * @return 
     */
    private <T> String convertStatToString(T stat) {
        return stat+"";
    }  
    
    /**
     * Returns all available statistics in a String array of a Cricket Player
     * Statistics are runs, matches, average, high score
     * @return 
     */
    public static String[] getAllStatsAsStrings(){
        String[] stringStats = new String[STATS_COUNT];
        stringStats[0]= "Runs";
        stringStats[1] = "Matches";
        stringStats[2] = "Average";
        stringStats[3] = "High Score";
        return stringStats;
    }
    
    /**
     * Returns the stat value as string
     * @param stat
     * @return 
     */
    public String getStat(String stat){
        if(stat.equals("Runs")){ return convertStatToString(getRuns());}
        else if(stat.equals("Matches")){return convertStatToString(getMatches());}
        else if(stat.equals("Average")){return convertStatToString(getAverage());}
        else{ return convertStatToString(getHighScore());}
    }

    // Setter and Getter Methods from here on
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public short getRuns() {
        return runs;
    }
    
    public void setRuns(short runs) {
        this.runs = runs;
    }

    public short getMatches() {
        return matches;
    }

    public void setMatches(short matches) {
        this.matches = matches;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public HighScore getHighScore() {
        return highScore;
    }

    public void setHighScore(short hs, boolean outStatus) {
        this.highScore.hs = hs;
        this.highScore.out = outStatus;
    }  
    
    public int getNoOfStats() {
        return STATS_COUNT;
    }    
    
    @Override
    /**
     * String representation of a cricket player
     * [Name, #Matches, Total Runs, Average, Highest Score]
     */
    public String toString(){
        return "\n"+name+", "+matches+", "+runs+", "+average+", "+highScore+"\n";
    }
    
    /**
     * Returns the cached Hash Code 
     * @return 
     */
    @Override
    public int hashCode(){
        int hash = hashCode;
        if(hash == 0){
            hash = 17;
            hash = 31*hash + name.hashCode();
            hash = 31*hash + (int)matches;
            hash = 31*hash + (int)runs;
            hash = 31*hash + ((Double) average).hashCode();
            hash = 31*hash + highScore.hashCode();
            hashCode = hash;
        }       
        return hash;
    }   
    
    
}
