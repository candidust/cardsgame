/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package API;

import java.util.Comparator;

/**
 * This class holds all type of comparators for cricket cards
 * @author BatmanLost
 */
public class CompareCards {    
    
    // Compare cards by appropriate statistic
    public static final RunsComparator BY_RUNS = new RunsComparator();
    public static final MatchesComparator BY_MATCHES = new MatchesComparator();
    public static final AverageComparator BY_AVERAGE = new AverageComparator();
    public static final HighScoreComparator BY_HIGH_SCORE = new HighScoreComparator();    
    
    /**
     * Compare cards by given stat
     * @param card1
     * @param card2
     * @param compareStat
     * @return 
     */
    public static int compare(CricketPlayerProfile card1, CricketPlayerProfile card2, String compareStat){
        if(compareStat.equals("Runs")){ return BY_RUNS.compare(card1, card2);}
        else if(compareStat.equals("Matches")){return BY_MATCHES.compare(card1, card2);}
        else if(compareStat.equals("Average")){return BY_AVERAGE.compare(card1, card2);}
        else{ return BY_HIGH_SCORE.compare(card1, card2);}
    }
    
    // Comparator classes
    private static class RunsComparator implements Comparator<CricketPlayerProfile>{
        @Override
        public int compare(CricketPlayerProfile card1, CricketPlayerProfile card2) {
            return card1.getRuns()>card2.getRuns() ? 1 : card1.getRuns()==card2.getRuns() ? 0 : -1;
        }
    }
    
    private static class MatchesComparator implements Comparator<CricketPlayerProfile>{
        @Override
        public int compare(CricketPlayerProfile card1, CricketPlayerProfile card2) {
            return card1.getMatches() > card2.getMatches() ? 1 : card1.getMatches() == card2.getMatches()  ? 0 : -1;
        }
    }
    
    private static class AverageComparator implements Comparator<CricketPlayerProfile>{
        @Override
        public int compare(CricketPlayerProfile card1, CricketPlayerProfile card2) {
            return Double.compare(card1.getAverage(), card2.getAverage());
        }
    }
    
    private static class HighScoreComparator implements Comparator<CricketPlayerProfile>{
        @Override
        public int compare(CricketPlayerProfile card1, CricketPlayerProfile card2){
            return card1.getHighScore().compareTo(card2.getHighScore());
        }
    }   
    
}
