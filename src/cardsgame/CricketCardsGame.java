/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cardsgame;

import API.ChallengerOpponent;
import API.CricketPlayerProfile;
import API.GamePlayer;
import DataMining.CricketPlayerCards;
import UI.GameView;
import java.io.FileNotFoundException;
import javax.swing.SwingUtilities;

/**
 * Controller class of the game
 * @author BatmanLost
 */
public class CricketCardsGame {   
    // Two players of the game, static because only 2 players are playing this game
    private static GamePlayer player1;   
    private static GamePlayer player2;    
    // UI manager
    private static GameView gameUI;
    
    private static ChallengerOpponent referee;    

    
    public CricketCardsGame(GamePlayer newPlayer1, GamePlayer newPlayer2 ){
        player1 = newPlayer1;   
        player2 = newPlayer2;  
        //Start the game with player1 as first challenger
        setChallengerOpponent();
        
        // inform referee about the players
        referee = new ChallengerOpponent(player1,player2);
        // update the UI manager
        gameUI = new GameView(player1,player2); 
        
        // start the game
        startGame();
    }

    /**
     * Start the game by displaying UI 
     */
    private void startGame() {        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {                
                gameUI.setVisible(true);
            }
        });
    }   
    
    /**
     * Sets player2 as challenger and player1 as opponent
     */
    private void setChallengerOpponent() {
        //player 1 gets ready to tackle challenge from opponent
        player1.orderTackleChallengeView();
        player1.setPreviousViewToTackleChallengeView();
        // player 2 waits till player1 is ready
        player2.orderWaitForOpponentView();
        player2.setPreviousViewToWaitForOpponentView();
    } 

    /**
     * Processes a new challenge and informs referee
     * @param player 
     */
    public static void takeChallengeFrom(GamePlayer player){
        referee.processNewChallenge(player);
        // displays challenge results
        displayWinnerLoser();                       
    }
    
    /**
     * Displays winner and loser appropriately
     */
    private static void displayWinnerLoser() {
        // if challenger has won
        if(referee.isChallengerWinner()){
            referee.getChallenger().orderChallengeWinnerView();
            referee.getOpponent().orderChallengeLoserView();
        }
        // if challenger lost
        else{
            referee.getOpponent().orderChallengeWinnerView();
            referee.getChallenger().orderChallengeLoserView();
        }
        reDrawUI();
    }
    
    /**
     * Confirms if a player is ready to tackle and informs other player to 
     *  proceed with the challenge
     * @param player 
     */
    public static void readyToTackle(GamePlayer player){        
        player.orderReadyToTackleView();
        getOtherPlayer(player).orderPlayerChallengeView();
        reDrawUI();        
    }
    
    /**
     * Returns the opponent of this player
     * @param gamePlayer
     * @return 
     */
    public static GamePlayer getOtherPlayer(GamePlayer gamePlayer){
        if(player1 == gamePlayer){ return player2;}
        else{ return player1;}
    }
    
    /**
     * Displays cards collection sorted by maximum runs first
     * @param player 
     */
    public static void viewCardsCollection(GamePlayer player){
        player.setSortBy("Runs");
        player.setMinFirst(false);
        player.orderCardsCollectionView();
        reDrawUI();
    }
    
    /**
     * Modifies the cards collection view as requested by the player
     * @param gamePlayer
     * @param stat
     * @param lowToHighChecked 
     */
    public static void modifyCollectionView(GamePlayer gamePlayer, String stat, boolean lowToHighChecked) {
        gamePlayer.setSortBy(stat);
        gamePlayer.setMinFirst(lowToHighChecked);
        gamePlayer.orderModifiedCardsCollectionView();
        reDrawUI();
    }
    
    /**
     * Takes back player to a state from where he clicked on collection button
     * @param player 
     */
    public static void goBackToPreviousView(GamePlayer player) {
        player.setViewRequired(player.getPreviousView());
        reDrawUI();
    }   
    
    
    /**
     * Informs the challenge result to players and asks to continue game
     * @param gamePlayer
     * @param result 
     */
    public static void continueGame(GamePlayer gamePlayer, String result){
        if(result.equals("Won")){ gamePlayer.orderWaitForOpponentView();}
        if(result.equals("Lost")){gamePlayer.orderTackleChallengeView();}
        // update strike cricket players of each game player after every challenge
        referee.updateStrikePlayers();
        reDrawUI();
    }
    
    /**
     * Returns the opponent's strike player
     * @param gamePlayer
     * @return 
     */
    public static CricketPlayerProfile getOpponentsStrikeCricketPlayer(GamePlayer gamePlayer){
        return getOtherPlayer(gamePlayer).getStrikeCricketPlayer();
    }
    
    /**
     * Re draws the game UI with corresponding changes made
     */
    public static void reDrawUI(){ gameUI.drawGameViewUI();}
    
    /**
     * Main class to start the game
     * @param args 
     */
    public static void main(String[] args) throws FileNotFoundException{        
        GamePlayer player1 = new GamePlayer(CricketPlayerCards.generateInputForPlayer1(),"Player 1");
        GamePlayer player2 = new GamePlayer(CricketPlayerCards.generateInputForPlayer2(),"Player 2");
        new CricketCardsGame(player1,player2);
    }
}
