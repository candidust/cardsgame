/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataMining;

import API.CricketPlayerProfile;
import API.CricketPlayerProfile.HighScore;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author BatmanLost
 */
public class CricketPlayerCards {    
    
    private static Set<CricketPlayerProfile> mainCardSet = new HashSet<CricketPlayerProfile>();
    
    /**
     * Returns N Cricket Player Profiles as an array
     * @param N
     * @return 
     */
    public static Set<CricketPlayerProfile> generate(){
        
        for(int i=0; i<6; i++){
            // csv file in which all the data is stored
            try{
                Scanner csvScanner = new Scanner(new File("resources\\CricketPlayerData"+i+".csv"));
                
                //skip headers of csv file
                csvScanner.nextLine();
                while(csvScanner.hasNextLine()){
                    // read a line from file
                    String data = csvScanner.nextLine();
                    // Split the line where ever comma is encountered
                    String[] stats = data.split(",");
                    // process each token in stats array as required
                    
                    String name = getPlayerNameFromCSV(stats[0]);
                    
                    short runs = getPlayerRunsFromCSV(stats[3]);
                    
                    short matches = getPlayerMatchesFromCSV(stats[2]);
                    
                    double average = getPlayerAverageFromCSV(stats[5]);
                    
                    HighScore hs = getPlayerHighScoreFromCSV(stats[4]);
                    
                    // build a new Cricket Player card
                    mainCardSet.add(new CricketPlayerProfile(name,runs,matches,average,hs));
                }
            }
            catch(Exception e){ System.out.println(e);}
        }        
        return mainCardSet;
       
    }
    
    /**
     * First loads all cards into @param mainCardSet static set and returns half of the cards
     * @return 
     */
    public static Set<CricketPlayerProfile> generateInputForPlayer1() throws FileNotFoundException{
        generate();
        Set<CricketPlayerProfile> inputSet = new HashSet<CricketPlayerProfile>();
        int N = mainCardSet.size();
        int i=0;
        Iterator it = mainCardSet.iterator();
        while( i<N/2){
            CricketPlayerProfile card = (CricketPlayerProfile) it.next();                  
            inputSet.add(card);
            it.remove();
            i++;      
        }
        return inputSet;
    } 
    /**
     * Returns remaining half cards from @param mainCardSet
     * @return 
     */
    public static Set<CricketPlayerProfile> generateInputForPlayer2(){        
        Set<CricketPlayerProfile> inputSet = new HashSet<CricketPlayerProfile>(mainCardSet);                
        return inputSet;
    } 
    
    // Helper methods to process tokens from csv file
    
    private static String getPlayerNameFromCSV(String token) {             
        return token.substring(1,token.length()-1);        
    }
    
    private static short getPlayerRunsFromCSV(String token) {
        return Short.parseShort(token.substring(1,token.length()-1));
    }

    private static short getPlayerMatchesFromCSV(String token) {
        return Short.parseShort(token.substring(1,token.length()-1));
    }

    private static double getPlayerAverageFromCSV(String token) {
        return Double.parseDouble(token.substring(1,token.length()-1));
    }

    private static HighScore getPlayerHighScoreFromCSV(String token) {
        String  stringHS = token.substring(1,token.length()-1);
        short hs = 0;
        boolean outStatus = true;
        if(stringHS.contains("*")){ 
            hs = Short.parseShort(stringHS.substring(0,stringHS.length()-1));
            outStatus = false; 
        } else{ hs = Short.parseShort(stringHS);}
        
        return new HighScore(hs,outStatus);
    }   
    
}
