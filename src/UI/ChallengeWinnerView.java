/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package UI;

import API.CricketPlayerProfile;
import API.GamePlayer;
import static UI.GamePlayerView.CRICKET_PLAYER_IMAGE_X_DIMENSION;
import static UI.GamePlayerView.CRICKET_PLAYER_IMAGE_Y_DIMENSION;
import static UI.GamePlayerView.CRICKET_PLAYER_VIEW_X_DIMENSION;
import cardsgame.CricketCardsGame;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * This class generates UI for a player after he has won a challenge
 * @author BatmanLost
 */
public class ChallengeWinnerView {
    
    // UI for this player
    private GamePlayer gamePlayer;
    
    public ChallengeWinnerView(GamePlayer player){        
        this.gamePlayer = player;
    }
    
    /**
     * Returns the card UI this player has won
     * @return 
     */
    public JPanel getChallengeWinnerView(){
        JPanel challengeWinnerView = new JPanel();
        challengeWinnerView.setLayout(new BoxLayout(challengeWinnerView,BoxLayout.Y_AXIS));
        addChallengeWinnerView(challengeWinnerView);
        return challengeWinnerView;
    }
    
    /**
     * Adds the appropriate elements
     * @param challengeWinnerView 
     */
    private void addChallengeWinnerView(JPanel challengeWinnerView) {
        
        challengeWinnerView.add(title());
        
        challengeWinnerView.add(cricketerView(gamePlayer.getRecentTransferCard()));       
       
        challengeWinnerView.add(continueButton());
        
        // adjust UI spaces
        challengeWinnerView.add(Box.createVerticalStrut(40));
        
        challengeWinnerView.add(Instructions());
        
        // adjust UI spaces
        challengeWinnerView.add(Box.createVerticalStrut(30));
    }
    
    private JPanel cricketerView(CricketPlayerProfile cricketPlayer){
        JPanel cricketerPanel = new JPanel();
        cricketerPanel.setLayout(new BoxLayout(cricketerPanel,BoxLayout.Y_AXIS));
        cricketerPanel.add(playerImage(cricketPlayer));
        cricketerPanel.add(playerName(cricketPlayer));
        cricketerPanel.add(playerStats(cricketPlayer));
        return cricketerPanel;
    }
    
    private JPanel playerName(CricketPlayerProfile cricketPlayer) {
        JPanel namePanel = new JPanel();
        // Align cricket player name at center
        JLabel playerName = new JLabel(cricketPlayer.getName(),SwingConstants.CENTER);
        playerName.setPreferredSize(new Dimension(CRICKET_PLAYER_VIEW_X_DIMENSION,20));
        namePanel.add(playerName);
        return namePanel;
    }
    
    /**
     * Display cricket player's data in order
     * @return
     */
    private JPanel playerStats(CricketPlayerProfile cricketPlayer) {
        
        // Panel with all stats in a row
        JPanel statsPanel = new JPanel();
        // stats of strike player
        HashMap<String,String> playerStats = cricketPlayer.getPlayerStats();
        int noOfStats = playerStats.size();
        
        for(String stat : playerStats.keySet()){
            
            // panel for each stat
            JPanel statPanel = new JPanel();
            // 2 rows, 1 column i.e name and value
            statPanel.setLayout(new GridLayout(2, 1, 5, 5));
            statPanel.setPreferredSize(new Dimension((CRICKET_PLAYER_VIEW_X_DIMENSION-10)/noOfStats,40));
            
            
            JLabel  statName = new JLabel(stat,SwingConstants.CENTER);
            JLabel statVal = new JLabel(playerStats.get(stat),SwingConstants.CENTER);
            
            // 1 grid of stat name & value
            statPanel.add(statName);
            statPanel.add(statVal);
            
            // enqueue grid to all stats holder
            statsPanel.add(statPanel);
            
        }
        return statsPanel;
    }
    
    /**
     * Add cricket player's image to display panel
     * @return
     */
    private JPanel playerImage(CricketPlayerProfile cricketPlayer) {
        JPanel panel = new JPanel();
        // path to player's picture in resources silhouette
        String path = "resources\\images\\"+cricketPlayer.getName()+".jpg";
        //String path = "resources\\images\\silhouette.jpg";
        JLabel image = new JLabel(new ImageIcon(path));
        image.setPreferredSize(new Dimension(CRICKET_PLAYER_IMAGE_X_DIMENSION,CRICKET_PLAYER_IMAGE_Y_DIMENSION));
        panel.add(image);
        return panel;
    }

    /**
     * Returns a button to continue game when clicked
     * @return 
     */
    private JPanel continueButton() {
        JPanel panel = new JPanel();
        JButton continueButton = new JButton("Continue Playing");
        continueButton.setActionCommand("Won");
        continueButton.addActionListener(new ContinuePlay());
        panel.add(continueButton);
        return panel;
    }

    private JPanel title() {
        JPanel panel = new JPanel();
        String title = "You WON with "+gamePlayer.getStrikeCricketPlayer().getName().toUpperCase();
        JLabel label = new JLabel(title);
        panel.add(label);
        return panel;
    }

    private JPanel Instructions() {
        JPanel panel = new JPanel();
        String instructions = "<html>This card has been added to your collection<br></br></html>";
        JLabel label= new JLabel(instructions);
        panel.add(label);
        return panel;
    }
    
    private class ContinuePlay implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ae) {
            CricketCardsGame.continueGame(gamePlayer, ae.getActionCommand());
        }
    }
}
