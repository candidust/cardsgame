/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import API.GamePlayer;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * UI, where game is played by two players.
 * @author BatmanLost
 */
public class GameView extends JFrame {
    
    // UI views of the game players
    private static GamePlayerView firstGamePlayerView;
    private static GamePlayerView secondGamePlayerView;
    
    /**
     * Initialize game UI for playing with two players of the game
     * @param player1
     * @param player2 
     */
    public GameView(GamePlayer player1, GamePlayer player2){
        firstGamePlayerView = new GamePlayerView(player1);
        secondGamePlayerView = new GamePlayerView(player2);
        drawGameViewUI();
    }

    /**
     * Add both player's views to this panel's view
     */
    public void drawGameViewUI() {
        // first clear content pane
        getContentPane().removeAll();        
        // create a panel for home view
        JPanel homeViewContentPane = new JPanel();        
        homeViewContentPane.setPreferredSize(new Dimension(1000,600));
        
        // add both player's views
        homeViewContentPane.add(firstGamePlayerView.getGamePlayerView());
        homeViewContentPane.add(secondGamePlayerView.getGamePlayerView());       
        
        // display
        add(homeViewContentPane);        

        pack();
        
        setTitle("Cards Game");        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }
}
