/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import API.CricketPlayerProfile;
import API.GamePlayer;
import static UI.GamePlayerView.CRICKET_PLAYER_IMAGE_X_DIMENSION;
import static UI.GamePlayerView.CRICKET_PLAYER_IMAGE_Y_DIMENSION;
import static UI.GamePlayerView.CRICKET_PLAYER_VIEW_X_DIMENSION;
import static UI.GamePlayerView.CRICKET_PLAYER_VIEW_Y_DIMENSION;
import cardsgame.CricketCardsGame;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

/**
 *
 * @author BatmanLost
 */
public class PlayerChallengeView {    
    
    private GamePlayer gamePlayer;
    
    public PlayerChallengeView(GamePlayer player){        
        this.gamePlayer = player;
    }
    
    /*************************************************************************
    *  Build the view for challenging the opponent.
    ***************************************************************************/
    
    public JPanel getChallengeView(){ 
        JPanel challengeView = new JPanel();
        challengeView.setLayout(new BoxLayout(challengeView,BoxLayout.Y_AXIS));
        addThrowDownAChallengeView(challengeView);
        return challengeView;
    } 
    
    
    /**
     * Build and return the UI of this player with his strike Cricket player 
     */
    private void addThrowDownAChallengeView(JPanel challengeView) {           

        // add cricket player card to game player view
        challengeView.add(addCricketPlayerPanel(gamePlayer.getStrikeCricketPlayer()));                        
       
    }
    
    /*****Helper Methods for building challenge view******/
    
    /**
     * Add strike cricket player data to display panel
     * @param cricketPlayerArea 
     */
    private JPanel addCricketPlayerPanel(CricketPlayerProfile cricketPlayer){ 
        JPanel cricketPlayerArea = new JPanel();
        cricketPlayerArea.setPreferredSize(new Dimension(CRICKET_PLAYER_VIEW_X_DIMENSION,CRICKET_PLAYER_VIEW_Y_DIMENSION));
        
        cricketPlayerArea.add(playerImage(cricketPlayer));       
        
        // Align cricket player name at center
        JLabel playerName = new JLabel(cricketPlayer.getName(),SwingConstants.CENTER);
        playerName.setPreferredSize(new Dimension(CRICKET_PLAYER_VIEW_X_DIMENSION,20));        
        cricketPlayerArea.add(playerName);
               
        cricketPlayerArea.add(playerStats(cricketPlayer));
        return cricketPlayerArea;
    }
    
    /**
     * Add cricket player's image to display panel
     * @return 
     */
    private JPanel playerImage(CricketPlayerProfile cricketPlayer) {
        JPanel panel = new JPanel();
        // path to player's picture in resources silhouette
        String path = "resources\\images\\"+cricketPlayer.getName()+".jpg";
        //String path = "resources\\images\\silhouette.jpg";
        JLabel image = new JLabel(new ImageIcon(path));
        image.setPreferredSize(new Dimension(CRICKET_PLAYER_IMAGE_X_DIMENSION,CRICKET_PLAYER_IMAGE_Y_DIMENSION));
        panel.add(image);
        return panel;
    }

    /**
     * Display cricket player's data in order
     * @return 
     */
    private JPanel playerStats(CricketPlayerProfile cricketPlayer) {
        // main container panel that holds player stats and a challenge button
        JPanel challengePanel =  new JPanel();        
        challengePanel.setLayout(new BoxLayout(challengePanel,BoxLayout.Y_AXIS));
        
        // Panel with all stats in a row
        JPanel statsPanel = new JPanel();  
        // container with play button
        JPanel challengeButtonPanel = new JPanel();
        // button to play game
        JButton challengeButton = new JButton("Select one choice");
        // Challenge the opponent 
        challengeButton.addActionListener(new NewChallenge(this.gamePlayer));
        challengeButtonPanel.add(challengeButton);
        
        ButtonGroup radioButtons = new ButtonGroup();
        
        // stats of strike player
        HashMap<String,String> playerStats = cricketPlayer.getPlayerStats();
        int noOfStats = playerStats.size();
        // populate stats area row one by one
        for(String stat: playerStats.keySet()){
            // panel for each stat
            JPanel statPanel = new JPanel();
            // 2 rows, 1 column i.e name and value
            statPanel.setLayout(new GridLayout(2, 1, 5, 5));
            statPanel.setPreferredSize(new Dimension((CRICKET_PLAYER_VIEW_X_DIMENSION-10)/noOfStats,40));
            
            // set action command and listener to radio button
            JRadioButton  statName = new JRadioButton(stat);            
            statName.setActionCommand(stat);
            statName.addActionListener(new UpdateChallengeButton(challengeButton));
            
            radioButtons.add(statName);
            
            JLabel statVal = new JLabel(playerStats.get(stat),SwingConstants.CENTER);
            
            // 1 grid of stat name & value
            statPanel.add(statName);
            statPanel.add(statVal);
            
            // enqueue grid to all stats holder
            statsPanel.add(statPanel);
        }
        
        // add panels built to main container
        challengePanel.add(statsPanel);
        challengePanel.add(challengeButtonPanel);
        
        return challengePanel;
    }   
   
    
     /**
     * Action Listener class for challenge button
     */
    private class NewChallenge implements ActionListener {
        //Store this gamePlayer as newChallenger
        private GamePlayer newChallenger;

        public NewChallenge(GamePlayer gamePlayer) {
            newChallenger = gamePlayer;             
        }

        @Override
        /**
         * Create a new challenge with this gamePlayer as challenger
         */
        public void actionPerformed(ActionEvent ae) {
            CricketCardsGame.takeChallengeFrom(newChallenger);            
        }
    }

    /**
     * Action Listener class for radio buttons
     */
    private class UpdateChallengeButton implements ActionListener {
        // modify challenge button when stat choice is modified
        private JButton challengeButton;

        private UpdateChallengeButton(JButton challengeButton) {            
            this.challengeButton =  challengeButton;
        }       

        @Override
        /**
         * Displays the stat selected for challenging and proceeds further
         * Saves the choice of selection in game players profile
         */
        public void actionPerformed(ActionEvent ae) {
            String challengeWithStat = ae.getActionCommand();
            challengeButton.setText("Challenge with "+challengeWithStat);            
            gamePlayer.setChallengeWith(challengeWithStat);
        }
    }
}
