/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import API.CricketPlayerProfile;
import API.GamePlayer;
import cardsgame.CricketCardsGame;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * This class generates UI to display player's cards
 * @author BatmanLost
 */
public class CardsCollectionView {
    // UI for this player
    private GamePlayer gamePlayer;   
    // sort by this stat
    private String stat;
    // sort by order, minimum comes first
    private boolean minFirst;
    
    public CardsCollectionView(GamePlayer player){
        this.gamePlayer = player;
        this.stat = player.getSortBy();
        this.minFirst = player.isMinFirst();
    }   
    
    /**
     * Returns a table displaying all cards and stats
     * @return 
     */
    public JPanel getCardsCollectionView(){
        JPanel cardsCollectionView = new JPanel();
        cardsCollectionView.setLayout(new BoxLayout(cardsCollectionView,BoxLayout.Y_AXIS));
        addCardsCollectionView(cardsCollectionView);
        return cardsCollectionView;
    }

    /**
     * Adds all required elements
     * @param cardsCollectionView 
     */
    private void addCardsCollectionView(JPanel cardsCollectionView) {
        cardsCollectionView.add(backButton());
        cardsCollectionView.add(sortOptions());
        cardsCollectionView.add(table(gamePlayer.getCardsSortedBy(stat, minFirst)));       
    }

    /**
     * displays all cricket players in a table
     * @param cardsSortedBy
     * @return 
     */
    private JPanel table(CricketPlayerProfile[] cardsSortedBy) {
        
        // build column names in an array 
        String[] columnNames = new String[5];
        columnNames[0] ="Name";
        // dynamically update column names
        System.arraycopy(CricketPlayerProfile.getAllStatsAsStrings(), 0, columnNames, 1, 4);
        
        //2d array with cricket player name and stats
        String[][] rowData = new String[gamePlayer.cardsCount()][columnNames.length];
        
        for(int i=0; i<rowData.length;i++){
            // for each row, get cricket player name as first colum
            rowData[i][0] = cardsSortedBy[i].getName();
            for(int j=1; j<columnNames.length; j++){
                // update data with corresponding column names
                rowData[i][j] = cardsSortedBy[i].getStat(columnNames[j]);
            }
        }
       
        // build table with column names and data
        JTable table = new JTable(rowData, columnNames);
        
        // add table to scroll panel
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setPreferredSize(new Dimension(400,420));
        
        // add scroll panel with table to a new panel and return it
        JPanel tablePanel = new JPanel();
        tablePanel.add(scrollPane);       
        
        return tablePanel;
    }    

    /**
     * Returns a button which takes player back to the view from where he clicked collection button
     * @return 
     */
    private JPanel backButton() {
        JPanel panel = new JPanel();        
        JButton backButton = new JButton("Go Back");
        backButton.addActionListener(new BackButtonListener());
        panel.add(backButton);         
        return panel;
    }

    /**
     * Give options to sort as the player wishes
     * @return 
     */
    private JPanel sortOptions() {
        JPanel panel = new JPanel();
        ButtonGroup stats = new ButtonGroup();
        JCheckBox lowToHigh = new JCheckBox("Low to High");
        lowToHigh.setSelected(minFirst);
        lowToHigh.addActionListener(new sortByOrder());
        
        for(String stat: CricketPlayerProfile.getAllStatsAsStrings()){
            JRadioButton radioBtn = new JRadioButton(stat);
            radioBtn.setActionCommand(stat);
            radioBtn.addActionListener(new sortByStat());
            if(this.stat.equals(stat)){ radioBtn.setSelected(true);}
            stats.add(radioBtn);
            panel.add(radioBtn);
        }        
        panel.add(lowToHigh);
        return panel;
    }
    
    // Simple action listener classes
    
    private class sortByOrder implements ActionListener {       

        @Override
        public void actionPerformed(ActionEvent ae) {
            JCheckBox lowToHighBox = (JCheckBox) ae.getSource();
            CricketCardsGame.modifyCollectionView(gamePlayer,gamePlayer.getSortBy(),lowToHighBox.isSelected());
        }
    }

    private class sortByStat implements ActionListener { 
        @Override
        public void actionPerformed(ActionEvent ae) {
            CricketCardsGame.modifyCollectionView(gamePlayer,ae.getActionCommand(),gamePlayer.isMinFirst());
        }
    }

    private class BackButtonListener implements ActionListener {
        
        @Override
        public void actionPerformed(ActionEvent ae) {
            CricketCardsGame.goBackToPreviousView(gamePlayer);
        }
    }
    
}
