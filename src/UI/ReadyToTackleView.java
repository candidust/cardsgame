/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import API.CricketPlayerProfile;
import API.GamePlayer;
import static UI.GamePlayerView.CRICKET_PLAYER_IMAGE_X_DIMENSION;
import static UI.GamePlayerView.CRICKET_PLAYER_IMAGE_Y_DIMENSION;
import static UI.GamePlayerView.CRICKET_PLAYER_VIEW_X_DIMENSION;
import static UI.GamePlayerView.CRICKET_PLAYER_VIEW_Y_DIMENSION;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.HashMap;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * This class generates UI for a player after he has selected 
 * his best defense and ready to tackle a challenge from opponent
 * @author BatmanLost
 */
public class ReadyToTackleView {
    // generate UI for this player
    private GamePlayer gamePlayer;
    
    public ReadyToTackleView(GamePlayer player){        
        this.gamePlayer = player;
    }
    
    /**
     * Returns the ready to tackle view after selecting the best option available
     * @return 
     */
    public JPanel getReadyToTackleView(){ 
        JPanel readyToTackleView = new JPanel();
        readyToTackleView.setLayout(new BoxLayout(readyToTackleView,BoxLayout.Y_AXIS));
        addReadyToTackleView(readyToTackleView);
        return readyToTackleView;
    }

    /**
     * build the view of selected cricket player for tackle
     * @param readyToTackleView 
     */
    private void addReadyToTackleView(JPanel readyToTackleView) {        
        // temp panel
        JPanel readyToTackleArea = new JPanel();
        readyToTackleArea.setPreferredSize(new Dimension(CRICKET_PLAYER_VIEW_X_DIMENSION,CRICKET_PLAYER_VIEW_Y_DIMENSION));               
        
        readyToTackleArea.add(cricketerView(gamePlayer.getStrikeCricketPlayer()));
        
        readyToTackleArea.add(Instructions());
        // add temp panel to main panel
        readyToTackleView.add(readyToTackleArea);
    }
    
    /**
     * Generates UI of a cricketer
     * @param cricketPlayer
     * @return 
     */
    private JPanel cricketerView(CricketPlayerProfile cricketPlayer){
        JPanel cricketerPanel = new JPanel();
        cricketerPanel.setLayout(new BoxLayout(cricketerPanel,BoxLayout.Y_AXIS));
        cricketerPanel.add(playerImage(cricketPlayer));
        cricketerPanel.add(playerName(cricketPlayer));
        cricketerPanel.add(playerStats(cricketPlayer));
        return cricketerPanel;
    }
    
    private JPanel playerName(CricketPlayerProfile cricketPlayer) {
        JPanel namePanel = new JPanel();
        // Align cricket player name at center
        JLabel playerName = new JLabel(cricketPlayer.getName(),SwingConstants.CENTER);
        playerName.setPreferredSize(new Dimension(CRICKET_PLAYER_VIEW_X_DIMENSION,20));        
        namePanel.add(playerName);
        return namePanel;
    }
    
    /**
     * Display cricket player's data in order
     * @return 
     */
    private JPanel playerStats(CricketPlayerProfile cricketPlayer) {        
        
        // Panel with all stats in a row
        JPanel statsPanel = new JPanel();   
        // stats of strike player
        HashMap<String,String> playerStats = cricketPlayer.getPlayerStats();
        int noOfStats = playerStats.size();
        
        String stat = gamePlayer.tackleWith();
        
        // panel for each stat
        JPanel statPanel = new JPanel();
        // 2 rows, 1 column i.e name and value
        statPanel.setLayout(new GridLayout(2, 1, 5, 5));
        statPanel.setPreferredSize(new Dimension((CRICKET_PLAYER_VIEW_X_DIMENSION-10)/noOfStats,40));
        
        
        JLabel  statName = new JLabel(stat,SwingConstants.CENTER);
        JLabel statVal = new JLabel(playerStats.get(stat),SwingConstants.CENTER);
        
        // 1 grid of stat name & value
        statPanel.add(statName);
        statPanel.add(statVal);
        
        // enqueue grid to all stats holder
        statsPanel.add(statPanel);
        
        
        return statsPanel;
    } 
    
    /**
     * Add cricket player's image to display panel
     * @return 
     */
    private JPanel playerImage(CricketPlayerProfile cricketPlayer) {
        JPanel panel = new JPanel();
        // path to player's picture in resources silhouette
        String path = "resources\\images\\"+cricketPlayer.getName()+".jpg";
        //String path = "resources\\images\\silhouette.jpg";
        JLabel image = new JLabel(new ImageIcon(path));
        image.setPreferredSize(new Dimension(CRICKET_PLAYER_IMAGE_X_DIMENSION,CRICKET_PLAYER_IMAGE_Y_DIMENSION));
        panel.add(image);
        return panel;
    }

    /**
     * Instructions to continue game
     * @return
     */
    private JPanel Instructions() {
        JPanel panel = new JPanel();
        String instructions = "<html>  <br></br><p> If the opponent challenges with "+ gamePlayer.tackleWith().toUpperCase()+", <br></br>this card will be used as defense.   <br></br>Otherwise a random card will be chosen from your collection. </p></html>";
        JLabel label = new JLabel(instructions);
        panel.add(label);
        return panel;
    }

    
}
