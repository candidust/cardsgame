/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import API.GamePlayer;
import cardsgame.CricketCardsGame;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * UI of a player playing the game
 * @author BatmanLost
 */
public class GamePlayerView extends JFrame{
    
    //Cricket player area x dimension
    public static final int CRICKET_PLAYER_VIEW_X_DIMENSION = 400;
    //Cricket player area y dimension
    public static final int CRICKET_PLAYER_VIEW_Y_DIMENSION = 500;
    //Cricket player image x dimension
    public static final int CRICKET_PLAYER_IMAGE_X_DIMENSION = 160;
    //Cricket player image y dimension
    public static final int CRICKET_PLAYER_IMAGE_Y_DIMENSION = 250; 
    
    //Player playing this game
    private final GamePlayer gamePlayer; 
    
    
    
    
    /**
     * Initialize this player's view
     * @param newPlayer 
     */
    public GamePlayerView(GamePlayer newPlayer){
        this.gamePlayer = newPlayer;         
        
    }   

    /**
     * Export this game player's view
     * @return 
     */
    public JPanel getGamePlayerView(){ 
        //game player's view with a background, border, name and collection button
        JPanel playerViewContentPane = new JPanel();
        playerViewContentPane.setLayout(new BoxLayout(playerViewContentPane,BoxLayout.Y_AXIS));
        
        playerViewContentPane.setBackground(Color.gray);
        playerViewContentPane.setBorder(BorderFactory.createEmptyBorder(20, 10, 20, 10));

        // add this player name
        playerViewContentPane.add(gamePlayerName()); 

        // show the no. of cards this player has
        playerViewContentPane.add(cardsCountView());         
        
        // add the view requested by the game player 
        playerViewContentPane.add(getRequestedView(gamePlayer.getViewRequired()));
                
        return playerViewContentPane;
    }  
    
    /**
     * Returns the view requested by game player
     * @param viewRequired
     * @return 
     */
    private JPanel getRequestedView(String viewRequired) {
        if(viewRequired.equals("PlayerChallengeView")){
            return new PlayerChallengeView(this.gamePlayer).getChallengeView();
        }
        else if(viewRequired.equals("WaitForOpponentView")){
            return new WaitForOpponentView(this.gamePlayer).getWaitForOpponentView();
        }
        else if(viewRequired.equals("ReadyToTackleView")){
            return new ReadyToTackleView(this.gamePlayer).getReadyToTackleView();
        }
        else if(viewRequired.equals("ChallengeWinnerView")){
            return new ChallengeWinnerView(this.gamePlayer).getChallengeWinnerView();
        }
        else if(viewRequired.equals("ChallengeLoserView")){
            return new ChallengeLoserView(this.gamePlayer).getChallengeLoserView();
        }
        else if(viewRequired.equals("CardsCollectionView")){
            return new CardsCollectionView(this.gamePlayer).getCardsCollectionView();
        }
        else{ return new TackleChallengeView(this.gamePlayer).getTackleChallengeView();}
    }
    
    /**
     * Returns name of this game player
     * @return 
     */
    private JLabel gamePlayerName() {      
        
        // Attach name of this player 
        JLabel gamePlayerName = new JLabel(gamePlayer.getName(),SwingConstants.LEFT);  
        gamePlayerName.setForeground(Color.WHITE); 
        gamePlayerName.setAlignmentX(Component.CENTER_ALIGNMENT);
        gamePlayerName.setFont(new Font("serif", Font.BOLD, 14));
        
        return gamePlayerName;        
    }
    
    /**
     * Returns total cards in collection
     * @return 
     */
    private JPanel cardsCountView() {
        JPanel panel = new JPanel();
        JButton noOfCards = new JButton("Collection : "+Integer.toString(this.gamePlayer.cardsCount()));
        noOfCards.addActionListener(new CardsCollectionButton());  
        // if player is already in collection view, disable collection button
        if(gamePlayer.getViewRequired().equals("CardsCollectionView")){
            noOfCards.setEnabled(false);
        }
        panel.add(noOfCards);             
        return panel;
    }  
    
    /**
     * Action Listener class for collection button
     */
    private class CardsCollectionButton implements ActionListener {       

        @Override
        public void actionPerformed(ActionEvent ae) {            
            CricketCardsGame.viewCardsCollection(gamePlayer);
        }
    }

    
}
