/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import API.CricketPlayerProfile;
import API.GamePlayer;
import static UI.GamePlayerView.CRICKET_PLAYER_IMAGE_X_DIMENSION;
import static UI.GamePlayerView.CRICKET_PLAYER_IMAGE_Y_DIMENSION;
import static UI.GamePlayerView.CRICKET_PLAYER_VIEW_X_DIMENSION;
import static UI.GamePlayerView.CRICKET_PLAYER_VIEW_Y_DIMENSION;
import cardsgame.CricketCardsGame;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

/**
 * This class generates UI for a player to prepare/tackle for a challenge 
 * from his opponent with his best chance to win
 * @author BatmanLost
 */
public class TackleChallengeView {
    // generate view for this player
    private GamePlayer gamePlayer;
    
    public TackleChallengeView(GamePlayer player){        
        this.gamePlayer = player;
    }
    
    public JPanel getTackleChallengeView(){ 
        JPanel tacklechallengeView = new JPanel();
        tacklechallengeView.setLayout(new BoxLayout(tacklechallengeView,BoxLayout.Y_AXIS));
        addTakeUpAChallengeView(tacklechallengeView);
        return tacklechallengeView;
    } 
    
    
    /**
     * Build and return UI to tackle the challenge from opponent
     * @param playerViewContentPane 
     */
    private void addTakeUpAChallengeView(JPanel playerViewContentPane){        
        // temp panel
        JPanel tackleChallengeArea = new JPanel();
        tackleChallengeArea.setPreferredSize(new Dimension(CRICKET_PLAYER_VIEW_X_DIMENSION,CRICKET_PLAYER_VIEW_Y_DIMENSION));               
        
        // add  silhouette image
        tackleChallengeArea.add(silhouetteImage());
        
        // add a label
        JLabel label = new JLabel("BEST STATS",SwingConstants.CENTER);
        label.setPreferredSize(new Dimension(CRICKET_PLAYER_VIEW_X_DIMENSION,20));                
        tackleChallengeArea.add(label);
        
        // Show the best options to be ready for the challenge
        tackleChallengeArea.add(showBestOptionsAvailable());  
        
        tackleChallengeArea.add(Instructions());
        
        // add temp panel to main panel
        playerViewContentPane.add(tackleChallengeArea);        
    }  
    
    /**
     * Returns the UI of his best cricketers
     * @return 
     */
    private JPanel showBestOptionsAvailable() {
        // main container which holds best available stats and 
        JPanel tacklePanel =  new JPanel();        
        tacklePanel.setLayout(new BoxLayout(tacklePanel,BoxLayout.Y_AXIS));       
        
        // stats panel which holds all stats and values
        JPanel bestStatsPanel =  new JPanel();
        
        // hash table of best cricketers of this player
        HashMap<String,CricketPlayerProfile> bestPlayers = gamePlayer.getBestPlayers();
        int noOfStats = bestPlayers.size();
        
        // container with button to play further
        JPanel tackleChallengeButtonPanel = new JPanel();
        // button to select the stat to tackle with
        JButton tackleButton = new JButton("I am Ready");
        // get ready to tackle
        tackleButton.addActionListener(new ReadyToTackleChallenge());
        tackleChallengeButtonPanel.add(tackleButton);
        
        ButtonGroup radioButtons = new ButtonGroup();
        
        // populate bestStatspanel
        for(String stat: bestPlayers.keySet()){
            CricketPlayerProfile bestPlayer = bestPlayers.get(stat);
            
            // panel for each stat
            JPanel statPanel = new JPanel();
            // 2 rows, 1 column i.e name and value
            statPanel.setLayout(new GridLayout(2, 1, 5, 5));
            statPanel.setPreferredSize(new Dimension((CRICKET_PLAYER_VIEW_X_DIMENSION-10)/noOfStats,40));
            
            // set action command and listener to radio button
            JRadioButton  statName = new JRadioButton(stat);     
            statName.setActionCommand(stat);
            statName.addActionListener(new tackleChallengeButton(bestPlayer));
            
            radioButtons.add(statName);
            
            JLabel statVal = new JLabel(bestPlayer.getStat(stat),SwingConstants.CENTER);

            
            statPanel.add(statName);
            statPanel.add(statVal);
            
            bestStatsPanel.add(statPanel);
        }
        // add best stats and tackle button
        tacklePanel.add(bestStatsPanel);
        tacklePanel.add(tackleChallengeButtonPanel);
        return tacklePanel;
    }
     
    /**
     * Adds silhouette of Sachin Tendulkar
     * @return 
     */
    private JPanel silhouetteImage() {
        JPanel panel = new JPanel();
        // path to player's picture in resources silhouette
        String path = "resources\\images\\silhouette.jpg";        
        JLabel image = new JLabel(new ImageIcon(path));
        image.setPreferredSize(new Dimension(CRICKET_PLAYER_IMAGE_X_DIMENSION,CRICKET_PLAYER_IMAGE_Y_DIMENSION));
        panel.add(image);
        return panel;
    }

    /**
     * Instructions to continue game
     * @return
     */
    private JPanel Instructions() {
        JPanel panel = new JPanel();
        String instructions = "<html>  <br></br><p> Please select one option.<br></br>You are lucky if the opponent selects the same stat as you. <br></br>It means you have selected your best defense. </p></html>";
        JLabel label = new JLabel(instructions);
        panel.add(label);
        return panel;
    }
    
    /**
     * Notifies the game controller if a player is ready to tackle challenge from his opponent
     */
    private class ReadyToTackleChallenge implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ae) {  
            // inform the contoller
            CricketCardsGame.readyToTackle(gamePlayer);
        }
    }

    /**
     * Updates the players defense stat i.e, tackle stat and his strike cricket player
     */
    private class tackleChallengeButton implements ActionListener {
        
        private CricketPlayerProfile bestPlayerSelected;

        public tackleChallengeButton(CricketPlayerProfile bestPlayer) {            
            this.bestPlayerSelected = bestPlayer;
        }

        @Override
        public void actionPerformed(ActionEvent ae){            
            gamePlayer.setTackleWith(ae.getActionCommand());
            gamePlayer.setStrikePlayer(bestPlayerSelected);
        }
    }
}
