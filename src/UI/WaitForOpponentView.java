/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import API.CricketPlayerProfile;
import API.GamePlayer;
import static UI.GamePlayerView.CRICKET_PLAYER_IMAGE_X_DIMENSION;
import static UI.GamePlayerView.CRICKET_PLAYER_IMAGE_Y_DIMENSION;
import static UI.GamePlayerView.CRICKET_PLAYER_VIEW_X_DIMENSION;
import static UI.GamePlayerView.CRICKET_PLAYER_VIEW_Y_DIMENSION;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.HashMap;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * This class generates UI for a player while opponent is getting ready for a tackle
 * @author BatmanLost
 */
public class WaitForOpponentView {
    // view for this player
    private GamePlayer gamePlayer;
    
    public WaitForOpponentView(GamePlayer player){        
        this.gamePlayer = player;
    }
    
    /**
     * Returns the UI for this player while opponent is getting ready for a tackle
     * Displays plain view of this player's strike cricket player
     * @return 
     */
    public JPanel getWaitForOpponentView(){ 
        JPanel waitForOpponentView = new JPanel();
        waitForOpponentView.setLayout(new BoxLayout(waitForOpponentView,BoxLayout.Y_AXIS));
        addWaitForOpponentView(waitForOpponentView);
        return waitForOpponentView;
    }    
    
    /**
     * Adds all required elements in order
     * @param waitForOpponentView 
     */
    private void addWaitForOpponentView(JPanel waitForOpponentView) {
        
        // temp panel
        JPanel waitForOpponentArea = new JPanel();
        waitForOpponentArea.setPreferredSize(new Dimension(CRICKET_PLAYER_VIEW_X_DIMENSION,CRICKET_PLAYER_VIEW_Y_DIMENSION));               
        
        waitForOpponentArea.add(playerImage(gamePlayer.getStrikeCricketPlayer()));
        
        waitForOpponentArea.add(playerName(gamePlayer.getStrikeCricketPlayer()));
        
        waitForOpponentArea.add(playerStats(gamePlayer.getStrikeCricketPlayer()));
        
        waitForOpponentArea.add(Instructions());
        
        // add temp panel to main panel
        waitForOpponentView.add(waitForOpponentArea);
    }
    
    /**
     * Display cricket player's data in order
     * @return 
     */
    private JPanel playerStats(CricketPlayerProfile cricketPlayer) {        
        
        // Panel with all stats in a row
        JPanel statsPanel = new JPanel();   
        // stats of strike player
        HashMap<String,String> playerStats = cricketPlayer.getPlayerStats();
        int noOfStats = playerStats.size();
        
        // populate stats area row one by one
        for(String stat: playerStats.keySet()){
            // panel for each stat
            JPanel statPanel = new JPanel();
            // 2 rows, 1 column i.e name and value
            statPanel.setLayout(new GridLayout(2, 1, 5, 5));
            statPanel.setPreferredSize(new Dimension((CRICKET_PLAYER_VIEW_X_DIMENSION-10)/noOfStats,40));
            
            
            JLabel  statName = new JLabel(stat,SwingConstants.CENTER); 
            JLabel statVal = new JLabel(playerStats.get(stat),SwingConstants.CENTER);
            
            // 1 grid of stat name & value
            statPanel.add(statName);
            statPanel.add(statVal);
            
            // enqueue grid to all stats holder
            statsPanel.add(statPanel);
        } 
        
        return statsPanel;
    } 
    
    /**
     * Add cricket player's image to display panel
     * @return 
     */
    private JPanel playerImage(CricketPlayerProfile cricketPlayer) {
        JPanel panel = new JPanel();
        // path to player's picture in resources silhouette
        String path = "resources\\images\\"+cricketPlayer.getName()+".jpg";
        //String path = "resources\\images\\silhouette.jpg";
        JLabel image = new JLabel(new ImageIcon(path));
        image.setPreferredSize(new Dimension(CRICKET_PLAYER_IMAGE_X_DIMENSION,CRICKET_PLAYER_IMAGE_Y_DIMENSION));
        panel.add(image);
        return panel;
    }

    /**
     * Displays cricket player's name
     * @param strikeCricketPlayer
     * @return 
     */
    private JPanel playerName(CricketPlayerProfile strikeCricketPlayer) {
        JPanel panel = new JPanel();
        // Align cricket player name at center
        JLabel playerName = new JLabel(strikeCricketPlayer.getName(),SwingConstants.CENTER);
        playerName.setPreferredSize(new Dimension(CRICKET_PLAYER_VIEW_X_DIMENSION,20));        
        panel.add(playerName);
        return panel;
    }

    /**
     * Instructions to continue game
     * @return 
     */
    private JPanel Instructions() {
        JPanel panel = new JPanel();
        String instructions = "<html> <br></br><br></br> <p> Please wait for the opponent to get ready </p></html>";
        JLabel label = new JLabel(instructions);
        panel.add(label);
        return panel;
    }
}
